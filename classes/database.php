<?php

class database
{

	private static $INSTANCE = null;
	private $mysqli,
	$HOST ='localhost',
	$USER ='root',
	$PASS ='',
	$DBNM ='latihan';

	public function __construct(){

		$this->mysqli = new mysqli($this->HOST, $this->USER, $this->PASS, $this->DBNM);

		if (mysqli_connect_error()) {
			die('Failure Connect to Database!!');
		}
	}

/*
single pattern, 
menguji Koneksi Agar tidak Double
*/

public static function getInstance()
{
	if (!isset(self::$INSTANCE)) {

		self::$INSTANCE = new database();
	}
	return self::$INSTANCE;
}

public function insert($table, $fields = array())
{
	  //get column
	$column = implode(", ", array_keys($fields));
	  //get values
	$valuesarrays = array();
	$i = 0;
	foreach ($fields as $key => $values) {
		if( is_int($values) ){

			$valuesarrays[$i] =$values;

		}else{
			$valuesarrays[$i] = "'" .$values. "'";
		}

		$i++;
	}

	$values = implode(", ", $valuesarrays);

	$query = "INSERT INTO $table ($column) VALUES ($values) ";

	if($this->mysqli->query($query)) return true;
	return false;

}
}

?>